
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a number to find its Factorial");
        int Number = scan.nextInt();
        int FactorialNum = FactorialProduct(Number);
        System.out.println("The Factorial of the Number is " +FactorialNum);
    }

    public static int FactorialProduct(int Num){
        int product = 1;
        while (Num > 1){
            product = product * Num;
            Num = Num - 1;
        };
        return product;
    }
}