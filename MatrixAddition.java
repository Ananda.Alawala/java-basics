package firstWeek;

import java.util.Scanner;

public class MatrixAddition {
public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	System.out.println("enter number of rows: ");
	int rows=scan.nextInt();
	System.out.println("enter number of columns: ");
	int columns=scan.nextInt();
	int[][] firstMatrix=new int[rows][columns];
	int[][] secondMatrix=new int[rows][columns];
	System.out.println("enter numbers in first matrix: ");
	for(int i=0;i<rows;i++) {
		for (int j=0;j<columns;j++) {
			firstMatrix[i][j]=scan.nextInt();
		}
	}
	System.out.println("enter numbers in second matrix: ");
	for(int i=0;i<rows;i++) {
		for (int j=0;j<columns;j++) {
			secondMatrix[i][j]=scan.nextInt();
		}
	}
	printAddedMatrix(firstMatrix, secondMatrix, rows, columns);
}
public static void printAddedMatrix(int[][] firstMatrix,int[][] secondMatrix,int rows,int columns ) {
	
	for(int i=0;i<rows;i++) {
		for (int j=0;j<columns;j++) {
			firstMatrix[i][j]=firstMatrix[i][j]+secondMatrix[i][j];
		}
	}
	System.out.println("Added matrix: ");
	for(int i=0;i<rows;i++) {
		for (int j=0;j<columns;j++) {
			System.out.print(firstMatrix[i][j]+"\t");
		}
		System.out.println("\n");
	}
}
}
