package firstWeek;

import java.util.Scanner;

public class MatrixMultiplication {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number of rows of first matrix: ");
		int frows=scan.nextInt();
		System.out.println("enter number of columns of first matrix: ");
		int columns=scan.nextInt();
		System.out.println("enter number of columns of second matrix: ");
		int scolumns=scan.nextInt();
		int[][] firstMatrix=new int[frows][columns];
		int[][] secondMatrix=new int[columns][scolumns];
		System.out.println("enter numbers in first matrix: ");
		for(int i=0;i<frows;i++) {
			for (int j=0;j<columns;j++) {
				firstMatrix[i][j]=scan.nextInt();
			}
		}
		System.out.println("enter numbers in second matrix: ");
		for(int i=0;i<columns;i++) {
			for (int j=0;j<scolumns;j++) {
				secondMatrix[i][j]=scan.nextInt();
			}
		}
		printProductMatrix(firstMatrix, secondMatrix, frows, columns,scolumns);
	}
	public static void printProductMatrix(int[][] firstMatrix,int[][] secondMatrix,int rows,int columns, int scolumns ) {
		int[][] productMatrix=new int[rows][scolumns];
		for(int i=0;i<rows;i++) {
			for (int j=0;j<scolumns;j++) {
				productMatrix[i][j]=0;
				for (int k = 0; k < columns; k++) {
					productMatrix[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
			}
		}
		}
		System.out.println("Product matrix: ");
		for(int i=0;i<rows;i++) {
			for (int j=0;j<scolumns;j++) {
				System.out.print(productMatrix[i][j]+"\t");
			}
			System.out.println("\n");
		}
	}

}
