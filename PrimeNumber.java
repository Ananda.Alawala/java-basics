package firstWeek;

import java.util.Scanner;

public class PrimeNumber {
public static void main(String[] args) {
	Scanner scan=new Scanner(System.in);
	System.out.println("enter the number to be checked");
	int numberToBeChecked=scan.nextInt();
	if(isPrime(numberToBeChecked)) {
		System.out.println("it is a prime number");
	}else {
		System.out.println("it is not a prime number");
	}
}
public static boolean isPrime(int numberToBeChecked) {
	boolean flag =true;
	if(numberToBeChecked==2) {
		return true;
	}else {
	for(int i=2;i<numberToBeChecked/2;i++) {
		if(numberToBeChecked%i==0) {
			flag=false;
		}
	}
	if(flag==true) {
	return true;
	}else {
		return false;
	}
	}
}
}
