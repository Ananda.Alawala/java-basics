
package firstWeek;

import java.util.Scanner;

public class PallindromeCheck {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("enter the String to be checked");
		String StringToBeChecked=scan.next();
		if(isPallindrome(StringToBeChecked)) {
			System.out.println("it is a pallindrom");
		}else {
			System.out.println("it is not a pallindrom");
		}
	}
	public static boolean isPallindrome(String StringToBeChecked) {
		boolean flag =true;
		String reverse = new StringBuilder(StringToBeChecked).reverse().toString();
        return StringToBeChecked.equals(reverse);
	}
	public static boolean isPalindromeCharWiseCheck(String StringToBeChecked)
    {
        String reverse = "";    
        int length = StringToBeChecked.length();        
        for (int i = length - 1; i >= 0; i-- )
            reverse = reverse + StringToBeChecked.charAt(i);        
        return StringToBeChecked.equals(reverse);
    }
}
